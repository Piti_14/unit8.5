import java.util.Scanner;

public class Recursive1{

    public static void main(String[] argv){

        int n;    
        Scanner value = new Scanner(System.in);
        System.out.println("How many numbers of the Fibonacci series do you want to know?");
        n = value.nextInt(); 
        value.close();
        
        System.out.println("Those numbers are:");

        for(int i = 0; i <= n; i++){
            System.out.println(i + ": " + fibonacci(i));
        }   
    }

    private static int fibonacci(int n){
        if(n == 0){
            return 0;       
        } else if(n == 1){
            return 1;
        } else{
            return fibonacci(n-1) + fibonacci(n-2);
        }
    }
}